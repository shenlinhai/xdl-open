package com.xdl.modules.system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.system.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xdl.modules.system.model.AddressListModel;
import com.xdl.modules.system.model.SysUserDepModel;
import com.xdl.modules.system.model.SysUserSysDepartModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    void updateUserDepart(@Param("username") String username, @Param("orgCode") String orgCode);

    List<SysUserDepModel> getDepNamesByUserIds(@Param("userIds")List<String> userIds);

    List<SysUser> deletelist();

    int revertLogicDeleted(@Param("userIds") String userIds, @Param("entity") SysUser entity);

    int deleteLogicDeleted(@Param("userIds") String userIds);

    IPage<SysUser> getUserByRoleId(Page page, @Param("roleId") String roleId, @Param("username") String username);

    /**
     *  根据部门Ids,查询部门下用户信息
     * @param page
     * @param departIds
     * @return
     */
    IPage<SysUser> getUserByDepIds(Page page, @Param("departIds") List<String> departIds, @Param("username") String username);


    List<SysUserSysDepartModel> getUserByOrgCode(Page page,@Param("model")  AddressListModel model);

    Integer getUserByOrgCodeTotal(@Param("model") AddressListModel model);
}
