package com.xdl.modules.wechat.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.wechat.entity.WxCoupon;
import com.xdl.modules.wechat.mapper.WxCouponMapper;
import com.xdl.modules.wechat.model.CouponModel;
import com.xdl.modules.wechat.service.WxCouponService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xdl.modules.wechat.vo.WxCouponVO;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-05
 */
@Service
public class WxCouponServiceImpl extends ServiceImpl<WxCouponMapper, WxCoupon> implements WxCouponService {

    @Override
    public IPage<WxCouponVO> queryPage(CouponModel model) {
        return this.baseMapper.queryPage(new Page<>(model.getPageNo(), model.getPageSize()),model.getTitle());
    }
}
