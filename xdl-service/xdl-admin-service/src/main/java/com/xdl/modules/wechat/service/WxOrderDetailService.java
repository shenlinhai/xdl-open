package com.xdl.modules.wechat.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.xdl.modules.wechat.entity.WxOrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.wechat.model.OrderModel;
import com.xdl.modules.wechat.vo.OrderDetailVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
public interface WxOrderDetailService extends IService<WxOrderDetail> {

    /**
     * 获取订单列表
     * @param model
     * @return
     */
    IPage<OrderDetailVO> getOrderList(OrderModel model);
}
