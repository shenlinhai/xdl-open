package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysDepart;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.system.model.DepartIdModel;
import com.xdl.modules.system.model.SysDepartTreeModel;

import java.util.List;

/**
 * <p>
 * 组织机构表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysDepartService extends IService<SysDepart> {

    /**
     * 获取部门
     * @param id
     * @return
     */
    List<SysDepart> queryUserDeparts(String id);

    List<SysDepartTreeModel> queryTreeList(String ids);

    List<DepartIdModel> queryDepartIdTreeList();

    boolean saveDepartData(SysDepart sysDepart, String username);

    void deleteBatchWithChildren(List<String> asList);

    List<SysDepartTreeModel> queryMyDeptTreeList(String departIds);

    List<String> getMySubDepIdsByDepId(String departIds);

    List<String> getSubDepIdsByDepId(String depId);

    List<SysDepartTreeModel> searchByKeyWord(String keyWord, String myDeptSearch, String departIds);

    boolean updateDepartDataById(SysDepart sysDepart, String username);
}
