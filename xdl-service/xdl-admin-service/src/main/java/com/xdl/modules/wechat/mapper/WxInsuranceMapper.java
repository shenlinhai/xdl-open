package com.xdl.modules.wechat.mapper;

import com.xdl.modules.wechat.entity.WxInsurance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
public interface WxInsuranceMapper extends BaseMapper<WxInsurance> {

}
