package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统日志表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
