package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.SysOssFile;
import com.xdl.modules.wechat.mapper.SysOssFileMapper;
import com.xdl.modules.wechat.service.SysOssFileService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * Oss File 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-19
 */
@Service
public class SysOssFileServiceImpl extends ServiceImpl<SysOssFileMapper, SysOssFile> implements SysOssFileService {

}
