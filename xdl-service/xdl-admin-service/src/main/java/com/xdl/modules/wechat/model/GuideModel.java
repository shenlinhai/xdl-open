package com.xdl.modules.wechat.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GuideModel {

    private String adress;

    private String location;

    private String adname;

    private String pname;

    private String name;

    public GuideModel(){}

    public GuideModel(PoisModel model){
        StringBuffer sb=new StringBuffer();
        sb.append(model.getPname()).append(model.getAdname()).append(model.getAddress());
        this.adress=sb.toString();
        this.location=model.getLocation();
        this.name= model.getName();
        this.adname=model.getAdname();
        this.pname=model.getPname();
    }

}
