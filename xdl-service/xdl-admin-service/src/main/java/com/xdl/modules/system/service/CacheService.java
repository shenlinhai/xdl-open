package com.xdl.modules.system.service;

public interface CacheService {

    /**
     * 缓存验证码
     *
     * @param realKey
     * @param lowerCaseCode
     * @param time
     * @return
     */
    boolean cacheRandomImage(String realKey, String lowerCaseCode, int time);

    /**
     * 获取验证码
     *
     * @param realKey
     * @return
     */
    Object getRandomImage(String realKey);


    /**
     * 获取缓存
     * @param realKey
     * @return
     */
    Object getCache(String realKey);

    void delKey(String realKey);

    boolean setCache(String key, String value, long time);
}
