package com.xdl.modules.system.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PositionModel extends BasePage{

    private String name;

    private String code;

    private String postRank;

}
