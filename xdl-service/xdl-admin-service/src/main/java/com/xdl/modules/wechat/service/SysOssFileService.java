package com.xdl.modules.wechat.service;

import com.xdl.modules.wechat.entity.SysOssFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * Oss File 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-19
 */
public interface SysOssFileService extends IService<SysOssFile> {

}
