package com.xdl.modules.system.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BasePage {

    private String column;

    private String order;

    private Integer pageNo;

    private Integer pageSize;

}
