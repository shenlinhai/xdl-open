package com.xdl.modules.system.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 包含 SysUser 和 SysDepart 的 Model
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysUserSysDepartModel {

    private String id;

    private String realname;

    private String workNo;

    private String post;

    private String telephone;

    private String email;

    private String phone;

    private String departId;

    private String departName;

    private String avatar;

}
