package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysUserDepart;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.system.model.DepartIdModel;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysUserDepartService extends IService<SysUserDepart> {

    List<DepartIdModel> queryDepartIdsOfUser(String userId);
}
