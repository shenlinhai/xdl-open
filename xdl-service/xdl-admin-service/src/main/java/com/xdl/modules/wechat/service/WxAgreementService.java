package com.xdl.modules.wechat.service;

import com.xdl.modules.wechat.entity.WxAgreement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
public interface WxAgreementService extends IService<WxAgreement> {

}
