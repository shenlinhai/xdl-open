package com.xdl.modules.wechat.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxOrderDetail extends Model<WxOrderDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 订单号
     */
    private String orderSn;

    /**
     * 支付订单号
     */
    private String transactionId;

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 活动id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long actId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 紧急联系人
     */
    private String contacts;

    /**
     * 血型
     */
    private Integer blood;

    /**
     * 预约地点
     */
    private String address;

    /**
     * 优惠券id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long couponId;

    /**
     * 预约日期
     */
    private LocalDateTime appointTime;

    /**
     * 是否参保
     */
    private Integer insurance;

    /**
     * 支付金额
     */
    private BigDecimal price;

    /**
     * 支付状态1-待支付2-已支付3-取消支付4-过期支付
     */
    private Integer payStatus;

    /**
     * 状态1-待付款2-待使用3-已使用4-售后
     */
    private Integer status;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新人
     */
    private String updateBy;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
