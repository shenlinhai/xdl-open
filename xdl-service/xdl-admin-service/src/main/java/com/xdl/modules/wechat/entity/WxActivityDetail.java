package com.xdl.modules.wechat.entity;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.common.utils.SnowflakeIdUtil;
import com.xdl.configuration.BaseContextHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 活动详情
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxActivityDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.INPUT)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id= SnowflakeIdUtil.nextId();

    @JsonSerialize(using = ToStringSerializer.class)
    private Long insurance;

    /**
     * 是否需要提前支付
     */
    private Integer needPay;

    /**
     * 活动名称
     */
    private String title;

    /**
     * 活动内容
     */
    private String actName;

    /**
     * 活动类型
     */
    private String classify;

    /**
     * 商户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long shopId;

    /**
     * 价格
     */
    private Double price;

    /**
     * 城市
     */
    private String city;

    /**
     * 地址
     */
    private String address;


    /**
     * 活动开始时间
     */
    private Date startDay;

    /**
     * 活动结束时间
     */
    private Date endDay;

    /**
     * 可预约天数
     */
    private Integer deadline;

    /**
     * 图片集合
     */
    private String images;

    /**
     * 活动主图
     */
    private String mainPic;

    /**
     * 图文
     */
    private String imageText;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 活动会员等级规则
     */
    private Integer menberLevel;


    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel=0;

    /**
     * 创建时间
     */
    private LocalDateTime createTime=LocalDateTime.now();

    /**
     * 更新时间
     */
    private LocalDateTime updateTime=LocalDateTime.now();

    /**
     * 创建人
     */
    private String createBy= BaseContextHandler.getUsername();

    /**
     * 更新人
     */
    private String updateBy= BaseContextHandler.getUsername();


}
