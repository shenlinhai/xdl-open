package com.xdl.modules.system.service;

import com.xdl.modules.system.model.BigDataModel;
import com.xdl.modules.system.model.MonthSalesModel;
import com.xdl.modules.system.model.StoreModel;

import java.util.List;

public interface BigDataService {

    /**
     * 获取
     * @return
     */
    BigDataModel getBigDataSource();

    /**
     * 获取每个月销售额
     * @return
     */
    List<MonthSalesModel> monthSales();

    /**
     * 获取各门店销售数据
     * @return
     */
    List<StoreModel> getStoreData();
}
