package com.xdl.modules.wechat.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.wechat.entity.WxBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xdl.modules.wechat.vo.WxBannerVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * banner图表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
public interface WxBannerMapper extends BaseMapper<WxBanner> {

    IPage<WxBannerVO> getPage(Page page,@Param("actName") String actName);
}
