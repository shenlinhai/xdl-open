package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysDepart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * <p>
 * 组织机构表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysDepartMapper extends BaseMapper<SysDepart> {

    List<SysDepart> queryUserDeparts(@Param("userId") String userId);

    List<String> getSubDepIdsByOrgCodes(@org.apache.ibatis.annotations.Param("orgCodes") String[] orgCodes);

    /**
     *  根据部门Id查询,当前和下级所有部门IDS
     * @param departId
     * @return
     */
    List<String> getSubDepIdsByDepId(@Param("departId") String departId);
}
