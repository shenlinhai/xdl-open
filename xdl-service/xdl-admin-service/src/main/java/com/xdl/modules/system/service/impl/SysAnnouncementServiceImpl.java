package com.xdl.modules.system.service.impl;

import com.xdl.modules.system.entity.SysAnnouncement;
import com.xdl.modules.system.mapper.SysAnnouncementMapper;
import com.xdl.modules.system.service.SysAnnouncementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统通告表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-06-28
 */
@Service
public class SysAnnouncementServiceImpl extends ServiceImpl<SysAnnouncementMapper, SysAnnouncement> implements SysAnnouncementService {

    @Override
    public int readAll() {
        return this.baseMapper.readAll();
    }
}
