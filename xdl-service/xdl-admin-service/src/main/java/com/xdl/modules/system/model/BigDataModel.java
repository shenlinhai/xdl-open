/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.system.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Author iron.guo
 * @Date 2022/5/20
 * @Description
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BigDataModel {

    /**
     * 订单总量
     */
    private String totnum;

    /**
     * 日订单量
     */
    private String daynum;

    /**
     * 日均销售额
     */
    private String avgze;

    /**
     * 销售总额
     */
    private String totze;

    /**
     * 周同比
     */
    private Double weekhb;

    /**
     * 日同比
     */
    private Double dayhb;

    /**
     * 月同比
     */
    private Double monthhb;

    /**
     * 支付总笔数
     */
    private String paynum;

    /**
     * 总支付金额
     */
    private String paytot;


}
