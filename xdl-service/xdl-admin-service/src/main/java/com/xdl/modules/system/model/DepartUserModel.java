package com.xdl.modules.system.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class DepartUserModel extends BasePage{

    private String depId;

    private String username;

}
