package com.xdl.modules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.common.utils.SnowflakeIdUtil;
import com.xdl.configuration.BaseContextHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统通告表
 * </p>
 *
 * @author iron guo
 * @since 2022-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysAnnouncement extends Model<SysAnnouncement> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.INPUT)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id= SnowflakeIdUtil.nextId();

    /**
     * 标题
     */
    private String titile;

    /**
     * 内容
     */
    private String msgContent;

    /**
     * 优先级（L低，M中，H高）
     */
    private String priority;

    /**
     * 消息类型1:通知公告2:系统消息
     */
    private String msgCategory;

    /**
     * 通告对象类型（USER:指定用户，ALL:全体用户）
     */
    private String msgType;

    /**
     * 业务id
     */
    private String busId;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel=0;

    private Integer readFlag;

    /**
     * 创建时间
     */
    private LocalDateTime createTime=LocalDateTime.now();

    /**
     * 更新时间
     */
    private LocalDateTime updateTime=LocalDateTime.now();

    /**
     * 创建人
     */
    private String createBy= BaseContextHandler.getUsername();

    /**
     * 更新人
     */
    private String updateBy=BaseContextHandler.getUsername();


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
