package com.xdl.modules.wechat.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GuideResult {

    private String info;

    private String status;

    private List<PoisModel> pois;



}
