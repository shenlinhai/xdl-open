package com.xdl.modules.wechat.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ShopModel {

    private String baseName;

    private GuideModel location;

}
