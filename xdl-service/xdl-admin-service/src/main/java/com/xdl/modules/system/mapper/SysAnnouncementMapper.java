package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysAnnouncement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * <p>
 * 系统通告表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-06-28
 */
public interface SysAnnouncementMapper extends BaseMapper<SysAnnouncement> {

    @Update("update sys_announcement set read_flag = '1' where read_flag = '0' ")
    int readAll();
}
