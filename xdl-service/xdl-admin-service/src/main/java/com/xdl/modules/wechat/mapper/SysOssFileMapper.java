package com.xdl.modules.wechat.mapper;

import com.xdl.modules.wechat.entity.SysOssFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Oss File Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-19
 */
public interface SysOssFileMapper extends BaseMapper<SysOssFile> {

}
