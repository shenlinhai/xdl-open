package com.xdl.modules.wechat.service.impl;

import com.xdl.modules.wechat.entity.WxAgreement;
import com.xdl.modules.wechat.mapper.WxAgreementMapper;
import com.xdl.modules.wechat.service.WxAgreementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
@Service
public class WxAgreementServiceImpl extends ServiceImpl<WxAgreementMapper, WxAgreement> implements WxAgreementService {

}
