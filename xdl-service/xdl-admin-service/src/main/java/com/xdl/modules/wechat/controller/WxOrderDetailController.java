package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.util.Func;
import com.xdl.common.response.ResponseResult;
import com.xdl.modules.wechat.entity.WxActivityDetail;
import com.xdl.modules.wechat.entity.WxOrderDetail;
import com.xdl.modules.wechat.entity.WxUser;
import com.xdl.modules.wechat.model.OrderModel;
import com.xdl.modules.wechat.service.WxOrderDetailService;
import com.xdl.modules.wechat.service.WxUserService;
import com.xdl.modules.wechat.vo.OrderDetailVO;
import lombok.AllArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-05-17
 */
@AllArgsConstructor
@BelugaController("/wx/order")
public class WxOrderDetailController {

    private WxOrderDetailService orderService;

    private WxUserService userService;

    /**
     * 获取订单列表
     *
     * @param model
     * @return
     */
    @PostMapping("/list")
    public ResponseResult<IPage<OrderDetailVO>> list(@RequestBody OrderModel model) {
        IPage<OrderDetailVO> result = orderService.getOrderList(model);
        return ResponseResult.Success(result);
    }


    /**
     * 更新新动力活动订单
     *
     * @param vo
     * @return
     */
    @PostMapping("edit")
    public ResponseResult<Boolean> edit(@RequestBody OrderDetailVO vo) {
        WxOrderDetail entity = orderService.getById(vo.getId());
        entity.setIdCard(vo.getIdCard());
        entity.setName(vo.getName());
        entity.setPhone(vo.getPhone());
        entity.setStatus(vo.getStatus());
        entity.setAppointTime(vo.getAppointTime() != null ? LocalDateTime.ofInstant(vo.getAppointTime().toInstant(), ZoneId.systemDefault()) : null);
        boolean update = orderService.updateById(entity);
        return ResponseResult.Success(update);
    }


    /**
     * 删除订单
     *
     * @param id
     * @return
     */
    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        boolean remove = orderService.removeById(id);
        return ResponseResult.Success(remove);
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @RequestMapping(value = "/deleteBatch", method = RequestMethod.DELETE)
    public ResponseResult<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        Assert.notNull(ids, "未选中订单！");
        String[] ls = ids.split(",");
        boolean removeByIds = orderService.removeByIds(Arrays.asList(ls));
        return ResponseResult.Success(removeByIds);

    }


    @GetMapping("getUser")
    public ResponseResult<?> getUser(@RequestParam String keyword) {
        QueryWrapper<WxUser> wrapper = new QueryWrapper<>();
        wrapper.lambda().like(WxUser::getRealName, keyword);
        List<WxUser> list = userService.list(wrapper);
        return ResponseResult.Success(list);
    }


}
