package com.xdl.modules.system.service.impl;

import com.xdl.modules.system.entity.SysPosition;
import com.xdl.modules.system.mapper.SysPositionMapper;
import com.xdl.modules.system.service.SysPositionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-09
 */
@Service
public class SysPositionServiceImpl extends ServiceImpl<SysPositionMapper, SysPosition> implements SysPositionService {

}
