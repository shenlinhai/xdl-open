package com.xdl.modules.wechat.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.xdl.common.response.ResponseCollection;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.RestUtil;
import com.xdl.common.utils.SnowflakeIdUtil;
import com.xdl.common.utils.StringUtils;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.constant.RestUrlConstant;
import com.xdl.modules.utils.GuideResponse;
import com.xdl.modules.wechat.entity.WxShop;
import com.xdl.modules.wechat.model.*;
import com.xdl.modules.wechat.service.WxShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 商户表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@RestController
@RequestMapping("/wx/shop")
public class WxShopController {

    @Value("${aliyun.guide.key}")
    private String guideKey;

    @Autowired
    private WxShopService shopService;

    @PostMapping("/list")
    public ResponseResult<IPage<WxShop>> list(@RequestBody ShopSaveModel model){
        QueryWrapper<WxShop> wrapper=new QueryWrapper<>();
        wrapper.lambda().like(StringUtils.isNotEmpty(model.getBaseName()),WxShop::getShopName,model.getBaseName())
                        .orderByAsc(WxShop::getCreateTime);
        IPage<WxShop> page = this.shopService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        return ResponseResult.Success(page);
    }


    /**
     * 获取所有小程序平台
     * @return
     */
    @GetMapping("getShopValue")
    public ResponseCollection<KVModel> getShopValue(){
        List<WxShop> list = this.shopService.list();
        List<KVModel> result=Lists.newArrayList();
        result.add(new KVModel().setKey("0").setValue("新动力小程序"));
        list.forEach(i->{
            KVModel model=new KVModel().setKey(String.valueOf(i.getId())).setValue(i.getShopName());
            result.add(model);
        });
        return ResponseCollection.Success(result);
    }

    /**
     * 获取所有的基地
     * @return
     */
    @GetMapping("getAll")
    public ResponseCollection<WxShop> getAll(){
        return ResponseCollection.Success(this.shopService.list());
    }


    /**
     * 高德地图获取基地经纬度
     * @param keyword
     * @return
     */
    @GetMapping("/getShopAdress")
    public ResponseCollection<GuideModel> getShopAdress(@RequestParam String keyword){
        GuideRestModel model=new GuideRestModel().setKey(guideKey).setKeywords(keyword);
        JSONObject params=JSONObject.parseObject(JSONObject.toJSONString(model));
        GuideResult result = RestUtil.get(RestUrlConstant.guide_url, params).toJavaObject(GuideResult.class);
        return ResponseCollection.Success(GuideResponse.response(result));
    }


    /**
     * 保存基地地址
     * @param model
     * @return
     */
    @PostMapping("/save")
    public ResponseResult<Boolean> save(@RequestBody ShopModel model){
        WxShop shop=new WxShop(model);
        shop.setId(SnowflakeIdUtil.nextId());
        return ResponseResult.Success(this.shopService.save(shop));
    }

    /**
     * 编辑基地
     * @param model
     * @return
     */
    @PostMapping("/edit")
    public ResponseResult<Boolean> edit(@RequestBody WxShop model){
        model.setUpdateTime(LocalDateTime.now());
        model.setUpdateBy(BaseContextHandler.getUsername());
        return ResponseResult.Success(this.shopService.updateById(model));
    }

    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        boolean remove = this.shopService.removeById(id);
        return ResponseResult.Success(remove);
    }







}
