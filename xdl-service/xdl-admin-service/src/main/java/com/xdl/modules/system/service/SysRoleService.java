package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysRoleService extends IService<SysRole> {

    boolean deleteRole(String id);
}
