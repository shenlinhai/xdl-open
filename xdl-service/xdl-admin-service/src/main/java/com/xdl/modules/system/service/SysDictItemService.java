package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysDictItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysDictItemService extends IService<SysDictItem> {

}
