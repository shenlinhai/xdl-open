package com.xdl.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xdl.modules.system.entity.SysDepart;
import com.xdl.modules.system.entity.SysUserDepart;
import com.xdl.modules.system.mapper.SysUserDepartMapper;
import com.xdl.modules.system.model.DepartIdModel;
import com.xdl.modules.system.service.SysDepartService;
import com.xdl.modules.system.service.SysUserDepartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
@Service
public class SysUserDepartServiceImpl extends ServiceImpl<SysUserDepartMapper, SysUserDepart> implements SysUserDepartService {

    @Autowired
    private SysDepartService sysDepartService;

    @Override
    public List<DepartIdModel> queryDepartIdsOfUser(String userId) {
        LambdaQueryWrapper<SysUserDepart> queryUDep = new LambdaQueryWrapper<SysUserDepart>();
        LambdaQueryWrapper<SysDepart> queryDep = new LambdaQueryWrapper<SysDepart>();
        queryUDep.eq(SysUserDepart::getUserId, userId);
        List<String> depIdList = new ArrayList<>();
        List<DepartIdModel> depIdModelList = new ArrayList<>();
        List<SysUserDepart> userDepList = this.list(queryUDep);
        if (userDepList != null && userDepList.size() > 0) {
            for (SysUserDepart userDepart : userDepList) {
                depIdList.add(userDepart.getDepId());
            }
            queryDep.in(SysDepart::getId, depIdList);
            List<SysDepart> depList = sysDepartService.list(queryDep);
            if (depList != null || depList.size() > 0) {
                for (SysDepart depart : depList) {
                    depIdModelList.add(new DepartIdModel().convertByUserDepart(depart));
                }
            }
            return depIdModelList;
        }
        return null;
    }
}
