package com.xdl.modules.wechat.mapper;

import com.xdl.modules.wechat.entity.WxShop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商户表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
public interface WxShopMapper extends BaseMapper<WxShop> {

}
