package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.StringUtils;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.wechat.entity.WxAgreement;
import com.xdl.modules.wechat.entity.WxBusiness;
import com.xdl.modules.wechat.model.AgreementModel;
import com.xdl.modules.wechat.model.BusinessModel;
import com.xdl.modules.wechat.service.WxBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-03
 */
@RestController
@RequestMapping("/wx/business")
public class WxBusinessController {

    @Autowired
    private WxBusinessService businessService;

    @PostMapping("/list")
    public ResponseResult<?> list(@RequestBody BusinessModel model){
        QueryWrapper<WxBusiness> wrapper=new QueryWrapper<>();
        wrapper.lambda().like(StringUtils.isNotEmpty(model.getBusiness()),WxBusiness::getBusiness,model.getBusiness())
                .orderByAsc(WxBusiness::getCreateTime);
        IPage<WxBusiness> page = this.businessService.page(new Page<>(model.getPageNo(), model.getPageSize()), wrapper);
        return ResponseResult.Success(page);
    }

    @PostMapping("save")
    public ResponseResult<Boolean> save(@RequestBody WxBusiness entity){
        return ResponseResult.Success(this.businessService.save(entity));
    }

    @PostMapping("edit")
    public ResponseResult<Boolean> edit(@RequestBody WxBusiness entity){
        entity.setUpdateTime(LocalDateTime.now());
        entity.setUpdateBy(BaseContextHandler.getUsername());
        return ResponseResult.Success(this.businessService.updateById(entity));
    }

    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        boolean remove = this.businessService.removeById(id);
        return ResponseResult.Success(remove);
    }
}
