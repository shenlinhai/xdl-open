package com.xdl.modules.system.service.impl;

import com.xdl.modules.system.entity.SysRole;
import com.xdl.modules.system.mapper.SysRoleMapper;
import com.xdl.modules.system.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteRole(String id) {
        //1.删除角色和用户关系
        this.baseMapper.deleteRoleUserRelation(id);
        //2.删除角色和权限关系
        this.baseMapper.deleteRolePermissionRelation(id);
        //3.删除角色
        this.baseMapper.deleteById(id);
        return true;
    }
}
