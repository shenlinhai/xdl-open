package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysTenant;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 多租户信息表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysTenantService extends IService<SysTenant> {

    List<SysTenant> queryEffectiveTenant(List<Integer> tenantIdList);

    boolean removeTenantById(String id);

    int countUserLinkTenant(String id);
}
