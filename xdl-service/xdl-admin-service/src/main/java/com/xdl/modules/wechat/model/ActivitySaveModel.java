package com.xdl.modules.wechat.model;

import com.xdl.modules.wechat.entity.WxActivityDetail;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ActivitySaveModel extends WxActivityDetail{

    private String fileList;

}
