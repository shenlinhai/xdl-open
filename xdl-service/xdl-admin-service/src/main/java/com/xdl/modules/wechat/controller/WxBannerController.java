package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sun.org.apache.xpath.internal.operations.Bool;
import com.xdl.common.response.ResponseCollection;
import com.xdl.common.response.ResponseResult;
import com.xdl.common.utils.SnowflakeIdUtil;
import com.xdl.common.utils.StringUtils;
import com.xdl.modules.wechat.entity.WxBanner;
import com.xdl.modules.wechat.model.BannerModel;
import com.xdl.modules.wechat.service.WxBannerService;
import com.xdl.modules.wechat.vo.WxBannerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * banner图表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@RestController
@RequestMapping("/wx/banner")
public class WxBannerController {
    @Autowired
    private WxBannerService bannerService;

    @PostMapping("/list")
    public ResponseResult<IPage<WxBannerVO>> list(@RequestBody BannerModel model){
        IPage<WxBannerVO> result=bannerService.getPage(model);
        return ResponseResult.Success(result);
    }


    @PostMapping("/save")
    public ResponseResult<Boolean> save(@RequestBody WxBanner model){
        boolean save = this.bannerService.save(model);
        return ResponseResult.Success(save);
    }


    @PostMapping("/edit")
    public ResponseResult<Boolean> edit(@RequestBody WxBanner model){
        boolean save = this.bannerService.updateById(model);
        return ResponseResult.Success(save);
    }

    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        boolean remove = this.bannerService.removeById(id);
        return ResponseResult.Success(remove);
    }


}
