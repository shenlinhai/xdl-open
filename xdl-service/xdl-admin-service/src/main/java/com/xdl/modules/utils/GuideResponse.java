package com.xdl.modules.utils;

import com.google.common.collect.Lists;
import com.xdl.modules.wechat.model.GuideModel;
import com.xdl.modules.wechat.model.GuideResult;

import java.util.List;

public class GuideResponse {

    public static List<GuideModel> response(GuideResult result){
        List<GuideModel> resultList= Lists.newArrayList();
        if(result.getStatus().equals("1")){
            result.getPois().forEach(i->{
                GuideModel m=new GuideModel(i);
                resultList.add(m);
            });
        }
        return resultList;
    }


}
