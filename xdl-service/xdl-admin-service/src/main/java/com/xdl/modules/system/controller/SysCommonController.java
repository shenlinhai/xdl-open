package com.xdl.modules.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.beluga.core.oss.AliOssBootService;
import com.beluga.core.oss.model.BelugaFile;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/sys/common")
public class SysCommonController {

    private AliOssBootService bootService;

    @PostMapping("upload")
    public JSONObject upload(@RequestParam("file") MultipartFile multipartFile){
        BelugaFile belugaFile = bootService.putFile(multipartFile);
        JSONObject result=new JSONObject();
        result.put("code",20000);
        result.put("message",belugaFile.getLink());
        result.put("data",null);
        return result;
    }

}
