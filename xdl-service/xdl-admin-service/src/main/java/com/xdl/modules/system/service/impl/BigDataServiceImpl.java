/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.system.service.impl;

import com.xdl.modules.system.mapper.BigDataMapper;
import com.xdl.modules.system.model.BigDataModel;
import com.xdl.modules.system.model.MonthSalesModel;
import com.xdl.modules.system.model.StoreModel;
import com.xdl.modules.system.service.BigDataService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author iron.guo
 * @Date 2022/5/20
 * @Description
 */
@Slf4j
@Service
@AllArgsConstructor
public class BigDataServiceImpl implements BigDataService {

    private BigDataMapper bigDataMapper;

    /**
     * 获取销售指标
     * @return
     */
    @Override
    public BigDataModel getBigDataSource() {
        return bigDataMapper.getBigDataSource();
    }

    @Override
    public List<MonthSalesModel> monthSales() {
        return bigDataMapper.monthSales();
    }

    @Override
    public List<StoreModel> getStoreData() {
        return bigDataMapper.getStoreData();
    }
}
