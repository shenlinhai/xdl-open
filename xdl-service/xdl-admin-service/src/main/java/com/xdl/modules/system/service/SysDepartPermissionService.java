package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysDepartPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 部门权限表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysDepartPermissionService extends IService<SysDepartPermission> {

    boolean saveDepartPermission(String departId, String permissionIds, String lastPermissionIds);
}
