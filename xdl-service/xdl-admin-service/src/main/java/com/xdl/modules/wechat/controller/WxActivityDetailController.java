package com.xdl.modules.wechat.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.beluga.core.util.Func;
import com.xdl.common.response.ResponseCollection;
import com.xdl.common.response.ResponseResult;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.wechat.entity.WxActivityDetail;
import com.xdl.modules.wechat.model.ActivityModel;
import com.xdl.modules.wechat.service.WxActivityDetailService;
import com.xdl.modules.wechat.vo.WxActivityDetailVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 活动详情 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
@Slf4j
@RestController
@RequestMapping("/wx/activity")
public class WxActivityDetailController {

    @Autowired
    private WxActivityDetailService activityDetailService;

    /**
     * 获取活动列表
     * @param model
     * @return
     */
    @PostMapping("/list")
    public ResponseResult<IPage<WxActivityDetailVO>> list(@RequestBody ActivityModel model){
        IPage<WxActivityDetailVO> result = this.activityDetailService.queryPage(model);
        return ResponseResult.Success(result);
    }

    /**
     * 模糊查询活动
     * @param keyword
     * @return
     */
    @GetMapping("getActByKey")
    public ResponseCollection<WxActivityDetail> getActByKey(@RequestParam String keyword){
        QueryWrapper<WxActivityDetail> wrapper=new QueryWrapper<>();
        wrapper.lambda().like(Func.isNotEmpty(keyword),WxActivityDetail::getTitle,keyword);
        List<WxActivityDetail> list = this.activityDetailService.list(wrapper);
        return ResponseCollection.Success(list);
    }

    /**
     * 新建活动
     * @param entity
     * @return
     */
    @PostMapping("save")
    public ResponseResult<Boolean> save(@RequestBody WxActivityDetail entity){
        return ResponseResult.Success(this.activityDetailService.save(entity));
    }

    /**
     * 编辑活动
     * @param entity
     * @return
     */
    @PostMapping("edit")
    public ResponseResult<Boolean> edit(@RequestBody WxActivityDetail entity){
        entity.setUpdateTime(LocalDateTime.now());
        entity.setUpdateBy(BaseContextHandler.getUsername());
        return ResponseResult.Success(this.activityDetailService.updateById(entity));
    }

    @DeleteMapping("/delete")
    public ResponseResult<Boolean> delete(@RequestParam Long id) {
        boolean remove = this.activityDetailService.removeById(id);
        return ResponseResult.Success(remove);
    }




}
