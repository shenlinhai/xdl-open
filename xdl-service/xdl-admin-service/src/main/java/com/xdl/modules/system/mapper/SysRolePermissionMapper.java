package com.xdl.modules.system.mapper;

import com.xdl.modules.system.entity.SysRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
