package com.xdl.modules.system.service;

import com.xdl.modules.system.entity.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-08
 */
public interface SysPermissionService extends IService<SysPermission> {

    List<SysPermission> queryByUser(String username);

    boolean editPermission(SysPermission permission);

    Boolean deletePermission(String id);

    boolean addPermission(SysPermission permission);
}
