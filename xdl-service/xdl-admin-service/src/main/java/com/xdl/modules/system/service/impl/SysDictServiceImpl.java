package com.xdl.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xdl.modules.system.entity.SysDict;
import com.xdl.modules.system.entity.SysDictItem;
import com.xdl.modules.system.mapper.SysDictMapper;
import com.xdl.modules.system.model.DictModel;
import com.xdl.modules.system.model.DuplicateCheckModel;
import com.xdl.modules.system.service.SysDictItemService;
import com.xdl.modules.system.service.SysDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
@Slf4j
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

    @Autowired
    private SysDictItemService dictItemService;

    @Override
    public Map<String, List<DictModel>> queryAllDictItems() {
        Map<String, List<DictModel>> res = new HashMap<String, List<DictModel>>();
        List<SysDict> ls = this.baseMapper.selectList(null);
        LambdaQueryWrapper<SysDictItem> queryWrapper = new LambdaQueryWrapper<SysDictItem>();
        queryWrapper.eq(SysDictItem::getStatus, 1);
        queryWrapper.orderByAsc(SysDictItem::getSortOrder);
        List<SysDictItem> sysDictItemList = dictItemService.list(queryWrapper);

        for (SysDict d : ls) {
            List<DictModel> dictModelList = sysDictItemList.stream().filter(s -> d.getId().equals(s.getDictId())).map(item -> {
                DictModel dictModel = new DictModel();
                dictModel.setText(item.getItemText());
                dictModel.setValue(item.getItemValue());
                return dictModel;
            }).collect(Collectors.toList());
            res.put(d.getDictCode(), dictModelList);
        }
        log.debug("-------登录加载系统字典-----" + res.toString());
        return res;
    }

    @Override
    public Long duplicateCheckCountSql(DuplicateCheckModel model) {
        return this.baseMapper.duplicateCheckCountSql(model);
    }

    @Override
    public Long duplicateCheckCountSqlNoDataId(DuplicateCheckModel model) {
        return this.baseMapper.duplicateCheckCountSqlNoDataId(model);
    }

    @Override
    public List<DictModel> getDictItems(String dictCode) {
        List<DictModel> result= Lists.newArrayList();
        QueryWrapper<SysDict> wrapper=new QueryWrapper<>();
        wrapper.lambda().eq(SysDict::getDictCode,dictCode);
        SysDict sysDict = this.baseMapper.selectOne(wrapper);
        List<SysDictItem> item=dictItemService.list(new LambdaQueryWrapper<SysDictItem>().eq(SysDictItem::getDictId,sysDict.getId()));
        item.forEach(i->{
            DictModel model=new DictModel(i.getItemValue(),i.getItemText());
            result.add(model);
        });
        return result;
    }

    @Override
    public List<SysDict> queryDeleteList() {
        return this.baseMapper.queryDeleteList();
    }

    @Override
    public void deleteOneDictPhysically(String id) {
        this.baseMapper.deleteOneById(id);
        this.dictItemService.remove(new LambdaQueryWrapper<SysDictItem>().eq(SysDictItem::getDictId,id));
    }

    @Override
    public boolean updateDictDelFlag(int i, String id) {
        this.baseMapper.updateDictDelFlag(i,id);
        return true;
    }


}
