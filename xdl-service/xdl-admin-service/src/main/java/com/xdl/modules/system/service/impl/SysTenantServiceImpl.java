package com.xdl.modules.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xdl.common.constants.CommonConstants;
import com.xdl.common.exception.BelugaException;
import com.xdl.modules.system.entity.SysTenant;
import com.xdl.modules.system.entity.SysUser;
import com.xdl.modules.system.mapper.SysTenantMapper;
import com.xdl.modules.system.service.SysTenantService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xdl.modules.system.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 多租户信息表 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
@Service
public class SysTenantServiceImpl extends ServiceImpl<SysTenantMapper, SysTenant> implements SysTenantService {

    @Autowired
    private SysUserService userService;

    @Override
    public List<SysTenant> queryEffectiveTenant(List<Integer> idList) {
        LambdaQueryWrapper<SysTenant> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(SysTenant::getId, idList);
        queryWrapper.eq(SysTenant::getStatus, Integer.valueOf(CommonConstants.STATUS_1));
        //此处查询忽略时间条件
        return super.list(queryWrapper);
    }

    @Override
    public boolean removeTenantById(String id) {
        // 查找出已被关联的用户数量
        int userCount = this.countUserLinkTenant(id);
        if (userCount > 0) {
            BelugaException.fatal("该租户已被引用，无法删除！");
        }
        return super.removeById(Integer.parseInt(id));
    }

    @Override
    public int countUserLinkTenant(String id) {
        LambdaQueryWrapper<SysUser> userQueryWrapper = new LambdaQueryWrapper<>();
        userQueryWrapper.eq(SysUser::getRelTenantIds, id);
        userQueryWrapper.or().like(SysUser::getRelTenantIds, "%," + id);
        userQueryWrapper.or().like(SysUser::getRelTenantIds, id + ",%");
        userQueryWrapper.or().like(SysUser::getRelTenantIds, "%," + id + ",%");
        // 查找出已被关联的用户数量
        return userService.count(userQueryWrapper);
    }
}
