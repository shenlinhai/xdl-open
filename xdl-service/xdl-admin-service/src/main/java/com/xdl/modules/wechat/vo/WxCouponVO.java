package com.xdl.modules.wechat.vo;

import com.xdl.common.utils.StringUtils;
import com.xdl.modules.wechat.entity.WxCoupon;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxCouponVO extends WxCoupon {

    private String shopName;

    public String getShopName() {
        return StringUtils.isEmpty(shopName) ? "新动力小程序" : shopName;
    }
}
