package com.xdl.modules.wechat.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GuideRestModel {

    private String keywords;

    private String key;

    private Integer offset=5;

    private Integer page=1;

    private String extensions="all";
}
