package com.xdl.modules.wechat.model;

import com.xdl.modules.system.model.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BusinessModel extends BaseModel {

    private String business;
}
