package com.xdl.modules.wechat.service;

import com.xdl.modules.wechat.entity.WxShop;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商户表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-30
 */
public interface WxShopService extends IService<WxShop> {

}
