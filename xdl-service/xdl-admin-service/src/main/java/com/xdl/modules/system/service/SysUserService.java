package com.xdl.modules.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.system.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.system.model.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-03-05
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * 更新用户部门
     * @param username
     * @param orgCode
     */
    void updateUserDepart(String username, String orgCode);

    List<String> getRole(String username);

    IPage<SysUser> queryPageList(SysUserModel model);

    Map<String, String> getDepNamesByUserIds(List<String> userIds);

    Boolean addUser(SysUserDto user);

    Boolean editUser(SysUser sysUser, String roles, String departs);

    Boolean changePassword(SysUser sysUser);

    List<SysUser> deletelist();

    Boolean revertLogicDeleted(List<String> asList, SysUser updateUser);

    Boolean deleteRecycleBin(List<String> userIds);

    IPage<SysUser> getUserByRoleId(Page<SysUser> page, RoleModel model);

    IPage<SysUser> getUserByDepIds(Page<SysUser> page, List<String> subDepids, String username);

    IPage<SysUserSysDepartModel> queryUserByOrgCode(AddressListModel model);
}
