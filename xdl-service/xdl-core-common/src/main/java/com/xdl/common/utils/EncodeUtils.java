package com.xdl.common.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * encode工具类
 * @author iron guo
 * @since 2021.11.27
 */
public class EncodeUtils {

    public static String urlEncode(String url) throws UnsupportedEncodingException {
        if(url == null) {
            return null;
        }

        final String reserved_char = ";/?:@=&";
        String ret = "";
        for(int i=0; i < url.length(); i++) {
            String cs = String.valueOf( url.charAt(i) );
            if(reserved_char.contains(cs)){
                ret += cs;
            }else{
                ret += URLEncoder.encode(cs, "utf-8");
            }
        }
        return ret.replace("+", "%20");
    }

    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println(urlEncode(urlEncode("土鸡蛋")));
    }

}
