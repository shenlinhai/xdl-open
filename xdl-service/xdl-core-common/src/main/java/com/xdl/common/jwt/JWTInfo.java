package com.xdl.common.jwt;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @ClassName JWTInfo
 * @Description TODO
 * @Author iron guo
 * @Date 2020/8/31 14:25
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
public class JWTInfo implements Serializable,IJWTInfo{

    private String userId;

    private String userName;

    private String openId;

    private String phone;

    private String password;




}
