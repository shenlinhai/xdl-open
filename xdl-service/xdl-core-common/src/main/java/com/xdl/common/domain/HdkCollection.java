package com.xdl.common.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class HdkCollection<T> {

    /**
     * 数据集合
     */
    private Collection<T> data;

    private Integer minId;

    public HdkCollection(Collection<T> data, Integer minId){
        this.data=data;
        this.minId=minId;
    }
}
