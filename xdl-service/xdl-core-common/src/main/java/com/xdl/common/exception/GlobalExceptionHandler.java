package com.xdl.common.exception;

import com.xdl.common.response.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName GlobalExceptionHandler
 * @Description 统一异常处理
 * @Author iron
 * @Date 2020/8/27 11:21
 * @Version 1.0
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 默认异常
     * @param e
     * @return
     * @throws Exception
     */
    @ResponseBody
    @ExceptionHandler(value = RestStatusException.class)
    public ResponseResult<?> restErrorHandler(RestStatusException e) throws Exception {
        log.error("统一异常抛出 {}",e.getMessage());
        return ResponseResult.Error(e.getCode(),e.getMessage());
    }

    @ResponseBody
    //@ExceptionHandler(value = Exception.class)
    public ResponseResult<?> defaultErrorHandler(Exception e) {
        log.error("统一异常抛出 {}",e.getMessage());
        return ResponseResult.Error(e.getMessage());
    }


}