package com.xdl.common.utils;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;


/**
 * @author iron guo
 * 生成邀请图片
 * @since 2022.01.26
 */
public class ImageGenUtil {


    public static void main(String[] args) {
        String backgroundPath = "C:\\Users\\jiedr\\Desktop\\bg.jpg";
        String qrCodePath = "C:\\Users\\jiedr\\Desktop\\ewm.jpg";
        String message01 ="扫描下方二维码，欢迎大家添加我的淘宝返利机器人";
        String message02 = "EWYGNYQ";
        String outPutPath="C:\\Users\\jiedr\\Desktop\\end.jpg";
        overlapImage(backgroundPath,qrCodePath,message01,message02,outPutPath);
    }


    public static String overlapImage(String backgroundPath,String qrCodePath,String message01,String message02,String outPutPath){
        try {
            //设置图片大小
            BufferedImage background = resizeImage(1120,2000, ImageIO.read(new File(backgroundPath)));
            BufferedImage qrCode = resizeImage(400,400,ImageIO.read(new File(qrCodePath)));
            Graphics2D g = background.createGraphics();
            g.setColor(Color.BLACK);
            g.setFont(new Font("微软雅黑",Font.PLAIN,40));
            //g.drawString(message01,530 ,190);
            g.drawString(message02,450 ,1915);
            //在背景图片上添加二维码图片
            g.drawImage(qrCode, 361, 1370, qrCode.getWidth(), qrCode.getHeight(), null);
            g.dispose();
            ImageIO.write(background, "jpg", new File(outPutPath));
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static BufferedImage resizeImage(int x, int y, BufferedImage bfi){
        BufferedImage bufferedImage = new BufferedImage(x, y, BufferedImage.TYPE_INT_RGB);
        bufferedImage.getGraphics().drawImage(
                bfi.getScaledInstance(x, y, Image.SCALE_SMOOTH), 0, 0, null);
        return bufferedImage;
    }



}
