package com.xdl.common.utils;

/**
 * 安全服务工具类
 *
 * @author iron guo
 * @since 2022.01.05
 */
public class SecurityUtils {

    /**
     * 生成MD5密码
     * @param password 密码
     * @return 加密字符串
     */
    public static String encryptPassword(String password) {
      return MD5Utils.getSaltMD5(password);
    }


    public static boolean verifyPassword(String password,String md5str){
        return MD5Utils.getSaltverifyMD5(password,md5str);
    }


}
