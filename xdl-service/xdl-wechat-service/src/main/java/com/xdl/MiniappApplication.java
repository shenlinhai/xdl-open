package com.xdl;

import com.beluga.core.annotation.BelugaBootApplication;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author iron guo
 * @since 2022.4.12
 * @des 新动力小程序启动
 */
@EnableCaching
@MapperScan("com.xdl.modules.mapper")
@BelugaBootApplication
public class MiniappApplication {
    public static void main(String[] args) {
        SpringApplication.run(MiniappApplication.class, args);
    }
}
