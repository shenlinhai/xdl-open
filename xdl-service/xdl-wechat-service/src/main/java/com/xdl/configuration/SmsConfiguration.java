package com.xdl.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "aliyun.sms")
public class SmsConfiguration {

    private String endpoint;

    private String accessKey;

    private String secretKey;


}
