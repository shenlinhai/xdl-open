package com.xdl.configuration;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

@EnableAsync // 配置在配置类上，可以将启动类上的注解去掉了
@Configuration
public class AsyncConfig implements AsyncConfigurer {

	@Bean()
	public ThreadPoolTaskExecutor asyncExecutor() {
		// spring线程池各个方法的作用自行学习，按需配置。
		ThreadPoolTaskExecutor executor=new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5);
		executor.setMaxPoolSize(10);
		executor.setKeepAliveSeconds(60);
		executor.setQueueCapacity(99999);
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		executor.setThreadNamePrefix("order-task-");// 看这里①
		executor.setAwaitTerminationSeconds(60);
		executor.setWaitForTasksToCompleteOnShutdown(true);
		return executor;
	}
	@Override
	public Executor getAsyncExecutor() {
		return asyncExecutor();
	}
	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		return (ex,method,params)->System.out.println(String.format("执行异步方法：%s错误，%s", params, ex)); // 在此演示中不是重点，请随意。
	}
}
