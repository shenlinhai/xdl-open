package com.xdl.configuration;

import com.xdl.common.constants.CommonConstants;
import com.xdl.common.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName BaseContextHandler
 * @Description 获取上下文
 * @Author iron
 * @Date 2020/8/31 13:31
 * @Version 1.0
 */
public class BaseContextHandler {

    public static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>();

    public static void set(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    public static Object get(String key){
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        return map.get(key);
    }

    public static String getUserID(){
        Object value = get(CommonConstants.JWT_KEY_USER_ID);
        return StringUtils.getObjectValue(value);
    }

    public static String getOpenId(){
        Object value = get(CommonConstants.JWT_KEY_OPEN_ID);
        return StringUtils.getObjectValue(value);
    }

    public static String getUsername(){
        Object value = get(CommonConstants.JWT_KEY_NAME);
        return StringUtils.getObjectValue(value);
    }

    public static String getPhone(){
        Object value = get(CommonConstants.JWT_KEY_PHONE);
        return StringUtils.getObjectValue(value);
    }

    public static void setUserID(String userID){
        set(CommonConstants.JWT_KEY_USER_ID,userID);
    }

    public static void setUsername(String username){
        set(CommonConstants.JWT_KEY_NAME,username);
    }

    public static void setOpenId(String openId){set(CommonConstants.JWT_KEY_OPEN_ID,openId);}

    public static void setPhone(String phone){set(CommonConstants.JWT_KEY_PHONE,phone);}

    public static void remove(){
        threadLocal.remove();
    }

}
