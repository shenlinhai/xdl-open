package com.xdl.modules.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.dto.ActivityClassifyDto;
import com.xdl.modules.dto.WxActivityDto;
import com.xdl.modules.entity.WxActivityDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xdl.modules.model.ActivityModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 活动详情 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-20
 */
public interface WxActivityDetailMapper extends BaseMapper<WxActivityDetail> {

    /**
     * 获取活动列表
     * @param page
     * @param model
     * @return
     */
    IPage<WxActivityDto> getList(Page page, @Param("model") ActivityModel model);

    /**
     * 根据id获取活动信息
     * @param id
     * @return
     */
    WxActivityDto getActById(@Param("id")Long id,@Param("userid")Long userid);

    /**
     * 获取活动类型
     * @return
     */
    List<ActivityClassifyDto> getActClassify();
}
