package com.xdl.modules.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.modules.util.RelativeDateFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 活动详情
 * </p>
 *
 * @author iron guo
 * @since 2022-04-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxActivityDetail extends BaseEntity<WxActivityDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * 活动名称
     */
    private String title;

    /**
     * 活动分类
     */
    private String classify;

    /**
     * 主图
     */
    private String mainPic;


    /**
     * 活动内容
     */
    private String actName;

    /**
     * 商户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long shopId;

    /**
     * 价格
     */
    private Double price;

    /**
     * 城市
     */
    private String city;

    /**
     * 地址
     */
    private String address;

    /**
     * 可预约天数
     */
    private Integer deadline;

    /**
     * 活动会员等级规则
     */
    private Integer menberLevel;

    /**
     * 图片集合
     */
    private String images;

    /**
     * 图文
     */
    private String imageText;

    /**
     * 活动开始时间
     */
    private LocalDateTime startDay;

    /**
     * 活动结束时间
     */
    private LocalDateTime endDay;

    /**
     * 状态
     */
    private Integer status;

}
