package com.xdl.modules.service;

import com.xdl.modules.dto.WechatLoginDto;
import com.xdl.modules.dto.WechatUserDto;
import com.xdl.modules.entity.WxUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 微信用户表 服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-21
 */
public interface WxUserService extends IService<WxUser> {

    /**
     * 微信登陆接口
     * @param user
     * @return
     */
    WechatUserDto loginWithWechat(WechatLoginDto user) throws Exception;
}
