///**
// * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
// * <p>
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// * <p>
// * http://www.apache.org/licenses/LICENSE-2.0
// * <p>
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package com.xdl.modules.task;
//
//import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
//import com.beluga.core.util.Func;
//import com.xdl.modules.dto.WxOrderToken;
//import com.xdl.modules.entity.WxOrderDetail;
//import com.xdl.modules.entity.WxUserTemp;
//import com.xdl.modules.service.CacheService;
//import com.xdl.modules.service.WxOrderDetailService;
//import com.xdl.modules.service.WxOwnerCouponService;
//import com.xdl.modules.service.WxUserTempService;
//import lombok.AllArgsConstructor;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Component;
//
///**
// * @Author iron.guo
// * @Date 2022/5/7
// * @Description
// */
//@Component
//@AllArgsConstructor
//public class AsynTask {
//
//    private WxOwnerCouponService ownerCouponService;
//
//    private WxUserTempService tempService;
//
//    private CacheService cacheService;
//
//    private WxOrderDetailService orderDetailService;
//
//
//    @Async()
//    public void orderTask(WxOrderDetail order){
//        /**
//         * 保存缓存设置半小时超时设计
//         */
//        String cache="XDLORDER_" + order.getOrderSn();
//        cacheService.setCache(cache, cache, 60 * 30);
//        orderDetailService.save(order);
//        if (Func.isNotEmpty(order.getCouponId())) {
//            //扣除优惠券
//            ownerCouponService.removeById(order.getCouponId());
//        }
//        //保存用户模版信息
//        QueryWrapper<WxUserTemp> wrapper = new QueryWrapper<>();
//        wrapper.lambda().eq(WxUserTemp::getIdCard, order.getIdCard());
//        WxUserTemp userTemp = tempService.getOne(wrapper);
//        if (Func.isEmpty(userTemp)) {
//            WxUserTemp temp = new WxUserTemp();
//            Func.copy(order, temp);
//            tempService.putUser(temp);
//        }
//    }
//

/**
 * 1。活动是否过期任务
 * 2。是否邀请成功定时任务
 * 3。优惠券是否过期定时任务
 */





//}
