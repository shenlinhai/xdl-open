package com.xdl.modules.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统通告表
 * </p>
 *
 * @author iron guo
 * @since 2022-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysAnnouncement extends BaseEntity<SysAnnouncement> {

    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    private String titile;

    /**
     * 内容
     */
    private String msgContent;

    /**
     * 优先级（L低，M中，H高）
     */
    private String priority;

    /**
     * 消息类型1:通知公告2:系统消息
     */
    private String msgCategory="2";

    /**
     * 通告对象类型（USER:指定用户，ALL:全体用户）
     */
    private String msgType="ALL";

    /**
     * 业务id
     */
    private String busId;

    /**
     * 0-未读 1-已读
     */
    private Integer readFlag=0;

}
