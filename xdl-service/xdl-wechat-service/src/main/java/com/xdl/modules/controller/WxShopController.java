package com.xdl.modules.controller;


import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseCollection;
import com.beluga.core.util.Func;
import com.google.common.collect.Lists;
import com.xdl.modules.dto.Location;
import com.xdl.modules.dto.WxShopDto;
import com.xdl.modules.service.WxShopService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

/**
 * <p>
 * 商户表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-05-10
 */
@AllArgsConstructor
@BelugaController("/wx/shop")
public class WxShopController {

    private WxShopService shopService;

    @PostMapping("list")
    public ResponseCollection<WxShopDto> list(@RequestBody Location location){
        List<WxShopDto> result= Lists.newArrayList();
        if(Func.isNotEmpty(location.getLat())&& Func.isNotEmpty(location.getLng())){
            result= shopService.getList(location.getLat(),location.getLng());
        }
        return ResponseCollection.Success(result);
    }





}
