package com.xdl.modules.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.beluga.core.annotation.BLimit;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseResult;
import com.xdl.modules.entity.WxBanner;
import com.xdl.modules.service.WxBannerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
/**
 * <p>
 * banner图表 前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-21
 */
@AllArgsConstructor
@BelugaController("/wx/banner")
public class WxBannerController {

    private WxBannerService bannerService;

    /**
     * banner列表
     * @return
     */
    @BLimit(value ="bannerList",limit = 10)
    @GetMapping("list")
    public ResponseResult<?> list(){
        QueryWrapper<WxBanner> wrapper=new QueryWrapper<>();
        wrapper.lambda().orderByAsc(WxBanner::getCreateTime);
        return ResponseResult.Success(bannerService.list(wrapper));
    }


}
