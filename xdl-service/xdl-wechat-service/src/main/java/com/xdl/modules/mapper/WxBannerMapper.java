package com.xdl.modules.mapper;

import com.xdl.modules.entity.WxBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * banner图表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-21
 */
public interface WxBannerMapper extends BaseMapper<WxBanner> {

}
