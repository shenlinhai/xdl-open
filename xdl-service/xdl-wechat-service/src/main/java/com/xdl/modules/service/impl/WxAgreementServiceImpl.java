package com.xdl.modules.service.impl;

import com.xdl.modules.entity.WxAgreement;
import com.xdl.modules.mapper.WxAgreementMapper;
import com.xdl.modules.service.WxAgreementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-26
 */
@Service
public class WxAgreementServiceImpl extends ServiceImpl<WxAgreementMapper, WxAgreement> implements WxAgreementService {

}
