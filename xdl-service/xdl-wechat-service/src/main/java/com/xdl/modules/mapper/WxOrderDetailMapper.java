package com.xdl.modules.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.dto.WxOrderDto;
import com.xdl.modules.entity.WxOrderDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xdl.modules.model.WxOrderModel;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-30
 */
public interface WxOrderDetailMapper extends BaseMapper<WxOrderDetail> {

    /**
     * 获取订单列表
     * @param page
     * @param model
     * @return
     */
    IPage<WxOrderDto> getOrderList(Page page, @Param("model") WxOrderModel model,@Param("userid") String userid );
}
