package com.xdl.modules.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-05-13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxMyInvite extends BaseEntity<WxMyInvite> {


    /**
     * 邀请人
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 被邀请人
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inviteId;

    /**
     * 是否生效
     */
    private Integer effect;

}
