package com.xdl.modules.service;

import com.xdl.modules.entity.WxInsurance;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-09
 */
public interface WxInsuranceService extends IService<WxInsurance> {

}
