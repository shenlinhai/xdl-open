package com.xdl.modules.mapper;

import com.xdl.modules.entity.WxUserTemp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-30
 */
public interface WxUserTempMapper extends BaseMapper<WxUserTemp> {

}
