package com.xdl.modules.service;

import com.xdl.modules.dto.WxOwnerCouponDto;
import com.xdl.modules.entity.WxOwnerCoupon;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xdl.modules.model.WxOwnerCouponModel;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-27
 */
public interface WxOwnerCouponService extends IService<WxOwnerCoupon> {

    /**
     * 领取优惠券
     * @param id
     * @return
     */
    Boolean putCoupon(Long id);

    /**
     * 获取我的优惠券
     * @param model
     * @return
     */
    List<WxOwnerCouponDto> getCouponList(WxOwnerCouponModel model);
}
