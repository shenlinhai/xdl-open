package com.xdl.modules.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.dto.WxCouponDto;
import com.xdl.modules.entity.WxCoupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-26
 */
public interface WxCouponMapper extends BaseMapper<WxCoupon> {

    /**
     * 获取优惠券列表
     * @param page
     * @param userid
     * @return
     */
    List<WxCouponDto> getCouponList(Page page,@Param("userid") Long userid);
}
