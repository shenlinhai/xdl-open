/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.controller;

import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseResult;
import com.google.common.collect.Lists;
import com.xdl.modules.dto.ActivityClassifyDto;
import com.xdl.modules.dto.FilterDto;
import com.xdl.modules.dto.KV;
import com.xdl.modules.service.WxActivityDetailService;
import com.xdl.modules.service.WxShopService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @Author iron.guo
 * @Date 2022/5/15
 * @Description
 */
@AllArgsConstructor
@BelugaController("/wx/filter")
public class ActivityFilterController {

    private WxShopService shopService;

    private WxActivityDetailService detailService;
    /**
     *
     * @return
     */
    @GetMapping("getFilterData")
    public ResponseResult<?> getFilterData(){
        List<FilterDto> result= Lists.newArrayList();
        List<KV> kv=shopService.getShopList();
        result.add(new FilterDto().setName("地区").setType("hierarchy-column").setSubmenu(kv));
        List<ActivityClassifyDto> actClassify = detailService.getActClassify();
        List<KV> classify=Lists.newArrayList();
        actClassify.forEach(i->{
            classify.add(new KV().setName(i.getNavName()).setValue(i.getCid()));
        });
        result.add(new FilterDto().setName("分类").setType("hierarchy-column").setSubmenu(classify));
        List<KV> orderby=Lists.newArrayList();
        orderby.add(new KV().setName("综合排序").setValue("createTime"));
        orderby.add(new KV().setName("价格升序").setValue("up"));
        orderby.add(new KV().setName("价格降序").setValue("down"));
        result.add(new FilterDto().setName("智能排序").setType("hierarchy-column").setSubmenu(orderby));
        return ResponseResult.Success(result);
    }




}
