package com.xdl.modules.mapper;

import com.xdl.modules.entity.SysAnnouncement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统通告表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-06-28
 */
public interface SysAnnouncementMapper extends BaseMapper<SysAnnouncement> {

}
