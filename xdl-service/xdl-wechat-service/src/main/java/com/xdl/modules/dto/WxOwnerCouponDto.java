/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @Author iron.guo
 * @Date 2022/4/27
 * @Description
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxOwnerCouponDto {

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 优惠券标题
     */
    private String title;

    /**
     * 优惠券金额
     */
    private Integer money;

    /**
     * 使用商铺
     */
    private String shopName;

    /**
     * 使用规则
     */
    private String rule;

    /**
     * 使用方式
     */
    private String ruleRemark;

    /**
     * 开始时间
     */
    @JsonFormat(pattern="yyyy.MM.dd",timezone="GMT+8")
    private LocalDateTime startDay;

    /**
     * 截止时间
     */
    @JsonFormat(pattern="yyyy.MM.dd",timezone="GMT+8")
    private LocalDateTime endDay;


}
