package com.xdl.modules.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BasePage {

    private Integer pageNo=1;

    private Integer pageSize=10;

}
