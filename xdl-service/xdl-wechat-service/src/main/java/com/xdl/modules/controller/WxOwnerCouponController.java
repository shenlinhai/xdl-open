package com.xdl.modules.controller;


import com.beluga.core.annotation.BLimit;
import com.beluga.core.annotation.BelugaController;

import com.beluga.core.response.ResponseCollection;
import com.beluga.core.response.ResponseResult;
import com.xdl.modules.dto.WxOwnerCouponDto;
import com.xdl.modules.model.BasePage;
import com.xdl.modules.model.WxOwnerCouponModel;
import com.xdl.modules.service.WxOwnerCouponService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-27
 */
@AllArgsConstructor
@BelugaController("/wx/myCoupon")
public class WxOwnerCouponController {

    private WxOwnerCouponService ownerCouponService;

    /**
     * 获取我的优惠券
     * @param model
     * @return
     */
    @BLimit(value ="ownerCouponList",limit = 10)
    @PostMapping("getCouponList")
    public ResponseResult<?> getCouponList(@RequestBody WxOwnerCouponModel model){
        List<WxOwnerCouponDto> result=ownerCouponService.getCouponList(model);
        return ResponseResult.Success(result);
    }


    /**
     * 领取优惠券
     * @param id
     * @return
     */
    @BLimit(value ="putCoupon",limit = 10)
    @GetMapping("putCoupon")
    public ResponseResult<?> putCoupon(@RequestParam Long id){
        return ResponseResult.Success(ownerCouponService.putCoupon(id));
    }



}
