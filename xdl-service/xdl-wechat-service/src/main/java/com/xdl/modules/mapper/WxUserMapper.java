package com.xdl.modules.mapper;

import com.xdl.modules.entity.WxUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 微信用户表 Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-21
 */
public interface WxUserMapper extends BaseMapper<WxUser> {

}
