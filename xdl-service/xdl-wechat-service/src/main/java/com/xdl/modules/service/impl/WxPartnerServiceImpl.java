package com.xdl.modules.service.impl;

import com.xdl.modules.entity.WxPartner;
import com.xdl.modules.mapper.WxPartnerMapper;
import com.xdl.modules.service.WxPartnerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-25
 */
@Service
public class WxPartnerServiceImpl extends ServiceImpl<WxPartnerMapper, WxPartner> implements WxPartnerService {

}
