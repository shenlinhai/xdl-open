package com.xdl.modules.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.dto.WxOwnerCouponDto;
import com.xdl.modules.entity.WxOwnerCoupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-04-27
 */
public interface WxOwnerCouponMapper extends BaseMapper<WxOwnerCoupon> {

    /**
     * 获取优惠券列表
     * @param
     * @param status
     * @return
     */
    List<WxOwnerCouponDto> getCouponList(Page page, @Param("status") Integer status,@Param("rule") Double rule,@Param("userid") String userid);
}
