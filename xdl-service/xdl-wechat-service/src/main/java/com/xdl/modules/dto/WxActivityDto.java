/**
 * Copyright (c) 2021-2028, iron.guo 郭成杰 (jiedreams@sina.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xdl.modules.dto;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.xdl.modules.util.RelativeDateFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author iron.guo
 * @Date 2022/4/20
 * @Description
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxActivityDto {

    /**
     * 主键
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @JsonSerialize(using = ToStringSerializer.class)
    private Long saveid;

    /**
     * 活动名称
     */
    private String title;

    /**
     * 活动分类
     */
    private String classify;

    /**
     * 主图
     */
    private String mainPic;


    /**
     * 活动内容
     */
    private String actName;

    /**
     * 商户id
     */
    private String shopName;

    /**
     * 价格
     */
    private Double price;

    /**
     * 活动会员等级规则
     */
    private Integer menberLevel;

    /**
     * 保险id insurance_id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long insuranceId;

    /**
     * 保险名称
     */
    private String insuranceName;

    /**
     * 保险费用
     */
    private BigDecimal insurancePrice;

    /**
     * 是否需要支付/预约
     */
    private Boolean needPay;

    /**
     * 城市
     */
    private String city;

    /**
     * 地址
     */
    private String address;

    /**
     * 经纬度
     */
    private String location;

    /**
     * 距离
     */
    private Double distance;

    /**
     * 可预约天数
     */
    private Integer deadline;

    /**
     * 图片集合
     */
    private String images;

    /**
     * 图文
     */
    private String imageText;

    /**
     * 活动开始时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date startDay;

    /**
     * 活动结束时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date endDay;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDel;

    /**
     * 创建时间
     */
    private Date createTime;

    private String cTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 更新人
     */
    private String updateBy;

    public String getcTime() {
        return RelativeDateFormat.format(this.createTime);
    }
}
