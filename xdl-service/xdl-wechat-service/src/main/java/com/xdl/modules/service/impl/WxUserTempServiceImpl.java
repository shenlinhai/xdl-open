package com.xdl.modules.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.beluga.core.util.Func;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.entity.WxUserTemp;
import com.xdl.modules.mapper.WxUserTempMapper;
import com.xdl.modules.service.WxUserTempService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-30
 */
@Service
//@CacheConfig(cacheNames = "user_cache")
public class WxUserTempServiceImpl extends ServiceImpl<WxUserTempMapper, WxUserTemp> implements WxUserTempService {

    /**
     * 保存用户常用信息
     *
     * @param temp
     * @return
     */
    @Override
    //@CachePut(cacheNames = "userTemp")
    public WxUserTemp putUser(WxUserTemp temp) {
        if(Func.isNotEmpty(temp) && Func.isNotEmpty(temp.getName())){
            this.baseMapper.insert(temp);
            return temp;
        }
        return null;
    }


    /**
     * 获取用户常用信息
     *
     * @return
     */
    @Override
    //@Cacheable(cacheNames = "userTemp")
    public List<WxUserTemp> pullTemp() {
        QueryWrapper<WxUserTemp> wrapper=new QueryWrapper<>();
        wrapper.lambda().eq(WxUserTemp::getUserId, BaseContextHandler.getUserID());
        return this.baseMapper.selectList(wrapper);
    }

    /**
     * 删除用户常用信息
     * @param id
     * @return
     */
    @Override
    //@CacheEvict(cacheNames="userTemp")
    public Boolean removeTemp(Long id) {
        this.baseMapper.deleteById(id);
        return true;
    }


}
