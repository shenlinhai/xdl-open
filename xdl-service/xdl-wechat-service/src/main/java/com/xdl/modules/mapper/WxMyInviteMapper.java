package com.xdl.modules.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.modules.dto.InviteDto;
import com.xdl.modules.entity.WxMyInvite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-13
 */
public interface WxMyInviteMapper extends BaseMapper<WxMyInvite> {

    /**
     * 获取我的邀请列表
     * @param page
     * @param userid
     * @return
     */
    IPage<InviteDto> getMyInviteList(Page page, @Param("userid") Long userid);
}
