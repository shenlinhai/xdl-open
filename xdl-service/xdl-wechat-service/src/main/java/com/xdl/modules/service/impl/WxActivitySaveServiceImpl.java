package com.xdl.modules.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xdl.configuration.BaseContextHandler;
import com.xdl.modules.dto.WxActivityDto;
import com.xdl.modules.entity.WxActivityDetail;
import com.xdl.modules.entity.WxActivitySave;
import com.xdl.modules.mapper.WxActivitySaveMapper;
import com.xdl.modules.model.BasePage;
import com.xdl.modules.service.WxActivityDetailService;
import com.xdl.modules.service.WxActivitySaveService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-04-22
 */
@Service
@AllArgsConstructor
public class WxActivitySaveServiceImpl extends ServiceImpl<WxActivitySaveMapper, WxActivitySave> implements WxActivitySaveService {

    /**
     * 获取用户收藏列表
     * @param page
     * @return
     */
    @Override
    public IPage<WxActivityDto> getSaveList(BasePage page) {
        Long userId= Long.valueOf(BaseContextHandler.getUserID());
        return this.baseMapper.getSaveList(new Page<>(page.getPageNo(),page.getPageSize()),userId);
    }
}
