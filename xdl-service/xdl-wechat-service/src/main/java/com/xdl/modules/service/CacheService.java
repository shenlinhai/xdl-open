package com.xdl.modules.service;

import java.util.Map;

public interface CacheService {

    void delKey(String realKey);

    boolean setCache(String key, String value, long time);

    void setOrderCache(String cache, Map<String, String> payinfo, long time);

    Map<Object, Object> getMapCache(String key);
}
