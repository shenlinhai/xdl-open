package com.xdl.modules.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableLogic;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author iron guo
 * @since 2022-04-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WxOwnerCoupon extends BaseEntity<WxOwnerCoupon> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    /**
     * 优惠券id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long couponId;

    /**
     * 1-未使用2-已使用3-已过期
     */
    private Integer status;

    /**
     * 开始时间
     */
    @JsonFormat(pattern="yyyy.MM.dd",timezone="GMT+8")
    private LocalDateTime startDay;

    /**
     * 截止时间
     */
    @JsonFormat(pattern="yyyy.MM.dd",timezone="GMT+8")
    private LocalDateTime endDay;

}
