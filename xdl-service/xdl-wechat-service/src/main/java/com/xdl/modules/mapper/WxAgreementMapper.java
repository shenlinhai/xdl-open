package com.xdl.modules.mapper;

import com.xdl.modules.entity.WxAgreement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author iron guo
 * @since 2022-05-26
 */
public interface WxAgreementMapper extends BaseMapper<WxAgreement> {

}
