package com.xdl.modules.service;

public interface WxMiniService {

    /**
     * 获取小程序二维码
     * @param scene
     * @param page
     * @param envVersion
     * @return
     */
    byte[] getWxQrcode(String scene, String page, String envVersion);
}
