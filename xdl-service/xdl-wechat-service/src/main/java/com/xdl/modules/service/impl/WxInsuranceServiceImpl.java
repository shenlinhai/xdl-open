package com.xdl.modules.service.impl;

import com.xdl.modules.entity.WxInsurance;
import com.xdl.modules.mapper.WxInsuranceMapper;
import com.xdl.modules.service.WxInsuranceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author iron guo
 * @since 2022-05-09
 */
@Service
public class WxInsuranceServiceImpl extends ServiceImpl<WxInsuranceMapper, WxInsurance> implements WxInsuranceService {

}
