package com.xdl.modules.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.beluga.core.annotation.BLimit;
import com.beluga.core.annotation.BelugaController;
import com.beluga.core.response.ResponseCollection;
import com.beluga.core.response.ResponseResult;
import com.xdl.modules.entity.WxPartner;
import com.xdl.modules.service.WxPartnerService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author iron guo
 * @since 2022-04-25
 */
@AllArgsConstructor
@BelugaController("/wx/partner")
public class WxPartnerController {

    private WxPartnerService partnerService;

    @BLimit(value ="partnerList",limit = 10)
    @GetMapping("list")
    public ResponseResult<?> list(){
        QueryWrapper<WxPartner> wrapper=new QueryWrapper<>();
        wrapper.lambda().orderByAsc(WxPartner::getCreateTime);
        return ResponseResult.Success(partnerService.list(wrapper));
    }


}
